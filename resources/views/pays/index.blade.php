@extends("template")
@section("titre")
Les pays
@endsection
@section("content")
<main class="container">
    <div class="row">
        @foreach ($lesPays as $unPays )
        <div class="card" style="width: 18rem;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{Str::upper($unPays->nom)}}</h5>
                <p class="card-text"></p>
                <a href="#" class="btn btn-primary">Consulter</a>

                @if (auth()->user() && auth()->user()->role->nom=="admin")
                <a href="/admin/pays/{{$unPays->id}}/edit" class="btn btn-primary">Modifier</a>
                <form action="/admin/pays/{{$unPays->id}}" method="post">
                    @csrf
                    @method("delete")
                    <button class="btn btn-primary">Supprimer</button>
                </form>
                @endif

            </div>
        </div>
        @endforeach
    </div>
</main>
@endsection
