@extends("template")
@section("titre")
Les pays
@endsection

@section("content")
<h1>Pays</h1>
{{-- @if (session()->get("success"))
<div class="alert alert-success">
    {{session()->get("success")}}
</div>
@endif --}}
<ul>
    @foreach ($dataPays as $unPays )
    <li>
        <div class="row my-2">
            <div class="col-2">
                <img class="responsive col-6 mx-auto" src="/storage/{{$unPays->drapeau ?? "pays/defaut.png" }}" alt="">
            </div>
            <div class="col">{{Str::ucfirst($unPays->nom)}}</div>
            <div class="col-8">
                <a class="btn btn-primary col-2 mx-1 mb-2" href="/pays/{{$unPays->id}}">Consulter</a>
                <a class="btn btn-primary col-2 mx-1 mb-2" href="/pays/{{$unPays->id}}/edit">Modifier</a>
                <form class="col-2 row mx-1" action="/pays/{{$unPays->id}}" method="post">
                    @method("delete")
                    @csrf
                    <button type="submit" class="btn btn-primary">Supprimer</button>
                </form>
            </div>
            <ul>
                @foreach ($unPays->destinations as $uneDestination )
                <li>
                    {{$uneDestination->nom}} {{$uneDestination->prix}}
                </li>
                @endforeach
            </ul>
        </div>
    </li>

    @endforeach
    <h2>Boucle 2</h2>
    <?php foreach ($dataPays as $unPays) {
        echo "<li>$unPays->nom</li>";
    } ?>
</ul>
@endsection
