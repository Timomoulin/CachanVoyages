@extends("template")
@section("titre")
Formulaire Pays
@endsection

@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Formulaire d'ajout d'un pays</h1>

        <form action="/pays" method="post" enctype="multipart/form-data">

            @csrf
            <div class="row mb-2">
                <label for="nom">Nom *</label>
                <input value="{{old("nom")}}" name="nom" required type="text" class="form-control" id="nom" placeholder="Enter nom">
            @error("nom")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="region">Region *</label>
                <input value="{{old("region")}}" name="region" required type="text" class="form-control" id="region" placeholder="Enter region">
            @error("region")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="capital">Capital *</label>
                <input value="{{old("capital")}}" name="capital" required type="text" class="form-control" id="capital" placeholder="Enter capital">
                @error("capital")
                <div class="alert alert-danger mt-1">{{$message}}</div>
                @enderror
            </div>

            <div class='row mb-2'>
                <label for='drapeau'>Drapeau</label>
                    <input value='{{old("drapeau")}}' accept="image/*" name='drapeau' required type='file' class="form-control" id="drapeau" placeholder="Enter drapeau">
                @error('drapeau')
                    <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection


