@extends("template")
@section("titre")
@endsection
@section("content")
<h1>
    {{$pays->nom}}
</h1>
<div class="mx-2 my-1">
    Capital : {{Str::ucfirst($pays->capital)}} <br>
    Region : {{Str::ucfirst($pays->region)}} <br>
    Destinations :
    <ul>
        @foreach ($pays->destinations as $uneDestination )
        <li> <a href="#">{{Str::ucfirst( $uneDestination->nom)}}</a></li>
        @endforeach
    </ul>
</div>
@endsection
