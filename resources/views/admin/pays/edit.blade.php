@extends("template")
@section("titre")
Formulaire Pays
@endsection

@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Modification de {{$pays->nom}}</h1>
        <form action="/pays/{{$pays->id}}" method="post" enctype="multipart/form-data">
            @method("put")
            @csrf
            <div class="row mb-2">
                <label for="nom">Nom *</label>
                <input value="{{old("nom") ?? $pays->nom }}" name="nom" required type="text" class="form-control" id="nom" placeholder="Enter nom">
            @error("nom")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="region">Region *</label>
                <input value="{{old("region")?? $pays->region}}" name="region" required type="text" class="form-control" id="region" placeholder="Enter region">
            @error("region")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="capital">Capital *</label>
                <input value="{{old("capital") ?? $pays->capital}}" name="capital" required type="text" class="form-control" id="capital" placeholder="Enter capital">
                @error("capital")
                <div class="alert alert-danger mt-1">{{$message}}</div>
                @enderror
            </div>

            <div class='row mb-2'>
                <label for='drapeau'>Drapeau</label>
                    <input value='{{old("drapeau")}}' accept="image/*" name='drapeau' type='file' class="form-control" id="drapeau" placeholder="Enter drapeau">
                @error('drapeau')
                    <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection
