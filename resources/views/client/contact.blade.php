@extends("template")
@section("titre")
Formulaire de contacte
@endsection
@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Formulaire de contacte</h1>
        <form action="/contacte" method="post">
            @csrf

            <div class='row mb-2'>
                <label for='email'>Email *</label>
                <input value='{{old("email")}}' name='email' required type='text' class="form-control" id="email"
                    placeholder="Votre email">
                @error('email')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

            <div class='row mb-2'>
                <label for='sujet'>Sujet *</label>
                <input value='{{old("sujet")}}' name='sujet' required type='text' class="form-control" id="sujet"
                    placeholder="Sujet">
                @error('sujet')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

            <div class='row mb-2'>
                <label for='texte'>Texte *</label>
                <textarea name='texte' required class="form-control" id="texte" placeholder="Enter texte">
                        {{old("texte")}}
                    </textarea>
                @error('texte')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

            {{-- /CAPTCHA --}}
            <div class='row mb-2'>
                <div class="captcha">
                    <span>{!! captcha_img() !!}</span>
                    <button type="button" class="btn btn-success btn-refresh"><i
                            class="bi bi-arrow-clockwise"></i></button>
                </div>
                <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                @if ($errors->has('captcha'))
                <span class="help-block">
                    <strong>{{ $errors->first('captcha') }}</strong>
                </span>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Envoyer</button>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(".btn-refresh").click(function(){
      $.ajax({
         type:'GET',
         url:'/refreshcaptcha',
         success:function(data){
            $(".captcha span").html(data.captcha);
         }
      });
    });


</script>
@endsection
