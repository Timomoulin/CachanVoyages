@extends("template")
@section("titre")
Les destinations
@endsection
@section("content")
<div class="container">
    <h1>Les destinations</h1>
    <div class="row">
        <form action="" method="get">
            <h2>Recherche de pays</h2>
            <div class='row mb-2'>
                <label for='nom'>Recherche par nom </label>
                <input value='{{old("nom")}}' name='nom'  type='text' class="form-control" id="nom"
                    placeholder="Enter nom">
                @error('nom')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>
            <div class='row mb-2'>
                <label for="">Pays</label>
                <select name="pays" id="" class="form-control">
                    <option selected disabled value="">Filtre par pays</option>
                    @foreach ($lesPays as $unPays )
                    <option value="{{$unPays->id}}">{{Str::ucfirst($unPays->nom)}}</option>
                    @endforeach
                </select>
                @error('pays')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>
            <button class="btn btn-secondary"> <i class="bi bi-search"></i> Recherche</button>
        </form>
    </div>
    <div class="d-flex justify-content-center my-3">{{ $lesDestinations->links() }}</div>

    <div class="row row-cols-2 row-cols-md-3 row-cols-lg-4">
        @if(count($lesDestinations)==0)
        <div class="alert alert-info">
            Pas de resultats !
        </div>
        @else
        @foreach ($lesDestinations as $uneDestination )
        <div class="card mx-auto my-2" style="width: 18rem;">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{Str::ucfirst($uneDestination->nom)}}</h5>
                <p class="card-text">
                    {{$uneDestination->pays->nom}}
                    Note : {{$uneDestination->calcNote()}}
                </p>
                @if($uneDestination->estDisponible==true)
                <div class="alert alert-success">
                    Est disponible
                </div>
                <a href="/destinations/{{$uneDestination->id}}" class="btn btn-primary">Consulter</a>
                @else
                <div class="alert alert-danger">
                    Est indisponible
                </div>
                @endif
            </div>
        </div>
        @endforeach
        @endif
    </div>
</div>
@endsection
