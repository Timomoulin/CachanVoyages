@extends("template")
@section("titre")

@endsection
@section("content")

<div class="container">
    <h1>{{Str::ucfirst($laDestination->nom)}}</h1>
    <div class="row row-cols-md-2 row-cols-1 mx-auto my-3">
        <div class="col">
            <h2>Les informations </h2>
            <p><b>Pays : </b> {{Str::ucfirst($laDestination->pays->nom)}}</p>
            <p><b>Prix de la place : </b> {{$laDestination->prix}} €</p>
            <p><b>Note :</b> 0/5
                @auth
                @if($laDestination->notes->contains(function($value,$key){return $value->id==auth()->user()->id;}))
                <br>
                <b>Deja voter</b>
                @else
            <form class="col-6" action="/client/destinations/{{$laDestination->id}}/vote" method="POST">
                @csrf
                <label for="vote">
                    <span>Votre note</span> <span id="valeurVote"></span>
                </label>
                <input id="vote" name="vote" required type="range" class="form-range" min="0" max="5" step="0.5"
                    id="vote">
                <button class="btn btn-primary">Voter</button>
            </form>
            @endif
            @endauth
            </p>
        </div>
        <div class="col">
            @guest
            <div class="alert alert-info">
                <b>
                    Pour ajouter un article a votre panier il faut <a href="/register"> vous inscrire </a>
                    ou <a href="/login">vous connecter</a>
                </b>
            </div>
            @endguest

            @auth
            <h2>Ajouter au panier</h2>
            <form class="mt-3 col-12 col-md-5" action="/client/panier" method="post">
                @csrf

                <input type="hidden" name="idDestination" value="{{$laDestination->id}}">
                <label for="nbPlaces">Nombre de places :</label>
                <input class="form-control my-2" id="nbPlaces" placeholder="nombre de places" type="number" min="0"
                    max="20" required name="places">
                <button class="btn btn-primary">Ajouter</button>
            </form>
            @endauth
        </div>

    </div>
    <hr class="my-2">
    <div class="row mx-auto row-cols-1">
        <h2>Commentaires </h2>
        <div class="container">
            @auth
            <div class="col-lg-10 mx-auto mb-3">
                <form action="/client/commentaires" method="post">
                    @csrf
                    <input type="hidden" name="destination_id" value="{{$laDestination->id}}">
                    <div class="form-floating my-2">
                        <textarea name="texte" required maxlength="255" minlength="4" class="form-control"
                            placeholder="Leave a comment here" id="floatingTextarea2"
                            style="height: 100px">{{old("commentaire")}}</textarea>
                        <label for="floatingTextarea2">Commentaire</label>
                    </div>
                    @error('texte')
                    <div class='alert alert-danger mt-1'>{{message}}</div>
                    @enderror
                    <button type="submit" class="btn btn-primary">Envoyer</button>
                </form>
            </div>
            @endauth
        </div>
        @foreach ($laDestination->commentaires->sortDesc()->forPage($nPage??1, 6) as $unCommentaire )
        <div class="alert alert-dark px-2 py-1 col-lg-10 mx-auto mt-1">
            <h5 class="alert-heading">{{Str::ucfirst($unCommentaire->autheur->prenom)}}</h4>
                <hr class="mt-1 mb-2">
                <p>{{$unCommentaire->texte}}</p>
                <p class="text-muted">{{$unCommentaire->created_at}} {{$unCommentaire->created_at !=
                    $unCommentaire->updated_at ? "(modifier)" : "" }}</p>
        </div>
        @endforeach
    </div>
</div>
<script>
    //Cible l'input pour le vote
let note= document.querySelector("#vote");
//Cible la div ou on veut afficher la valeur
let divValeur= document.querySelector("#valeurVote")

//Creation d'un ecouter d'evenement quand la valeur de l'input change

note.addEventListener("input",function(){
    divValeur.innerText=note.value;
})
</script>
@endsection
