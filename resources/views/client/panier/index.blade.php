@extends("template")
@section("titre")
Votre panier
@endsection
@section("content")
<div class="container">
<h1>Votre panier</h1>
<a class="btn btn-primary" href="/destinations">Retour aux destinations</a>
<table class="table">
    <thead>
        <tr>
            <th>N°</th>
            <th>Destination</th>
            <th>Prix</th>
            <th>Nombre de places</th>
            <th>Sous Total</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($panier as $uneLigne )
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$uneLigne->name}}</td>
            <td>{{$uneLigne->price}} €</td>
            <td> {{$uneLigne->quantity}} </td>
            <td>{{$uneLigne->quantity*$uneLigne->price}} €</td>
            <td>

<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal{{ $uneLigne->id}}" >Modifier</button>

<div class="modal fade" id="exampleModal{{ $uneLigne->id}}" tabindex="-1" aria-labelledby="exampleModalLabel{{ $uneLigne->id}}" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel{{ $uneLigne->id}}">Modification du panier</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="/client/panier/{{ $uneLigne->id}}" method="post">
            @csrf
            @method("put")
          <div class="mb-3">
            <label for="destination-name" class="col-form-label">Destination:</label>
            <input value="{{ $uneLigne->name}}" disabled type="text" class="form-control" id="destination-name">
          </div>
          <div class="mb-3">
            <label for="destination-prix" class="col-form-label">Prix/place:</label>
            <input value="{{ $uneLigne->price}} €" disabled type="text" class="form-control" id="destination-prix">
          </div>
          <div class="mb-3">
            <label for="inputPlaces" class="col-form-label">Nombre de place:</label>
            <input type="number" value="{{$uneLigne->quantity}}" required min="0" max="20" class="form-control" id="inputPlaces" name="places"/>
          </div>
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          <button class="btn btn-primary">Modifier</button>
        </form>
      </div>

    </div>
  </div>
</div>
                <form action="/client/panier/{{$uneLigne->id}}" method="post">
                    @method("delete")
                    @csrf
                <button class="btn btn-danger">Supprimer</button>
                </form>

            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<form action="/client/panier" method="post">
@method("delete")
@csrf
<button class="btn btn-danger">Vider le panier</button>
</form>
@auth
<form action="/client/commandes" method="post">
    @csrf
    <button class="btn btn-success">Commander</button>
</form>
@endauth

</div>

@endsection
