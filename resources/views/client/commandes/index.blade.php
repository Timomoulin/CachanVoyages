@extends("template")
@section("titre")
{{__('voyage.commandes')}}
@endsection
@section("content")
    <div class="container">
        <h1>{{__('voyage.commandes')}}</h1>


        <table class="table table-stripped">
            <thead>
                <tr>
                    <th>N°</th>
                    <th>Date</th>
                    <th>Statut</th>
                    <th>Destinations</th>
                    <th>Prix Total</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($commandes as $uneCommande )
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$uneCommande->created_at}}</td>
                    <td>{{$uneCommande->etat}}</td>
                    <td>
                        <ul>
                            @foreach ($uneCommande->lignes as $uneLigne  )
                            <li class="mb-2"><b>{{$uneLigne->nom}}</b> <br>
                            Places : {{$uneLigne->pivot->nbPlaces}}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td>{{$uneCommande->calcTotal()}}</td>
                    <td>Actions</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
