@extends("template")
@section("titre")
@endsection
@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Creation d'une destination
        </h1>
        <form action="/destinations" method="post">
            @csrf
            <div class='row mb-2'>
                <label for='nom'>Nom *</label>
                <input value='{{old("nom")}}' name='nom' required type='text' class="form-control" id="nom"
                    placeholder="Enter nom">
                @error('nom')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

            <div class='row mb-2'>
                <label for='prix'>prix *</label>
                <input value='{{old("prix")}}' min="1" name='prix' required type='number' class="form-control" id="prix"
                    placeholder="Enter prix">
                @error('prix')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

            <div class='row mb-2'>
                <label for='disponible'>Disponibilité *</label>
                <input value='{{old("disponible")??true}}' name='estDisponible' type='checkbox' id="disponible"
                    placeholder="Enter disponible">
                @error('disponible')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

            <div class='row mb-2'>
                <label for='pays'>Pays *</label>
                <select name='pays_id' required class="form-control" id="pays">
                    <option disabled selected value="">Choisir un pays</option>
                    @foreach ($lesPays as $unPays )
                    <option value="{{$unPays->id}}">{{Str::ucfirst($unPays->nom)}}</option>
                    @endforeach
                </select>
                @error('pays')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection
