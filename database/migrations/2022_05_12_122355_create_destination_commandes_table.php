<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDestinationCommandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('destination_commande', function (Blueprint $table) {
            $table->foreignId("destination_id")->constrained("destinations")->onDelete("cascade");
            $table->foreignId("commande_id")->constrained("commandes")->onDelete("cascade");
            $table->primary(["destination_id","commande_id"]);
            $table->integer("nbPlaces");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('destination_commandes');
    }
}
