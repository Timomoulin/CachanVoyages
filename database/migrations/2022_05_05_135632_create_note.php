<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('note', function (Blueprint $table) {
            $table->foreignId("user_id")->constrained("utilisateurs","id")->onDelete("cascade");
            $table->foreignId("destination_id")->constrained("destinations","id")->onDelete("cascade");
            $table->primary(["user_id","destination_id"]);
            $table->float("vote");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('note');
    }
}
