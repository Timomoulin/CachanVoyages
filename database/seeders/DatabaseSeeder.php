<?php

namespace Database\Seeders;

use App\Models\Commande;
use App\Models\Commentaire;
use App\Models\Destination;
use App\Models\DestinationCommande;
use App\Models\Note;
use App\Models\Pays;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $pays1=Pays::create([
            "nom"=>"France",
            "drapeau"=>"drapeau1.jpg",
            "region"=>"europe",
            "capital"=>"paris"]);

            $pays2=Pays::create([
                "nom"=>"Egypt",
                "drapeau"=>"drapeau2.jpg",
                "region"=>"affrique",
                "capital"=>"caire"]);

        $destination1=Destination::create([
            "nom"=>"Croisière sur le Nil",
            "prix"=>249.99,
            "estDisponible"=>true,
            "pays_id"=>$pays2->id
        ]);

        $destination2=Destination::create([
            "nom"=>"Visite du Caire",
            "prix"=>349.99,
            "estDisponible"=>true,
            "pays_id"=>$pays2->id
        ]);
        Destination::factory(50)->create();
        // $destination2=Destination::create([
        //     "nom"=>"Visite du Caire1",
        //     "prix"=>349.99,
        //     "estDisponible"=>true,
        //     "pays_id"=>$pays2->id
        // ]);
        // $destination3=Destination::create([
        //     "nom"=>"Visite du Caire2",
        //     "prix"=>349.99,
        //     "estDisponible"=>true,
        //     "pays_id"=>$pays2->id
        // ]);
        // $destination4=Destination::create([
        //     "nom"=>"Visite du Caire3",
        //     "prix"=>349.99,
        //     "estDisponible"=>true,
        //     "pays_id"=>$pays2->id
        // ]);
        // $destination5=Destination::create([
        //     "nom"=>"Visite du Caire5",
        //     "prix"=>349.99,
        //     "estDisponible"=>true,
        //     "pays_id"=>$pays2->id
        // ]);
        // $destination6=Destination::create([
        //     "nom"=>"Visite du Caire6",
        //     "prix"=>349.99,
        //     "estDisponible"=>true,
        //     "pays_id"=>$pays2->id
        // ]);

        // $destination7=Destination::create([
        //     "nom"=>"Visite du Caire7",
        //     "prix"=>349.99,
        //     "estDisponible"=>true,
        //     "pays_id"=>$pays2->id
        // ]);
        // $destination8=Destination::create([
        //     "nom"=>"Visite du Caire8",
        //     "prix"=>349.99,
        //     "estDisponible"=>true,
        //     "pays_id"=>$pays2->id
        // ]);

        // $destination9=Destination::create([
        //     "nom"=>"Visite du Caire9",
        //     "prix"=>349.99,
        //     "estDisponible"=>true,
        //     "pays_id"=>$pays2->id
        // ]);


        $roleAdmin=Role::create(["nom"=>"admin"]);
        $roleClient=Role::create(["nom"=>"client"]);

        $utilisateur1=User::create([
            "prenom"=>"timothée",
            "nom"=>"moulin",
            "password"=>bcrypt("timomoulin@msn.com1"),
            "email"=>"timomoulin@msn.com",
            "role_id"=>$roleAdmin->id
        ]);

        Note::create([
            "user_id"=>$utilisateur1->id,
            "destination_id"=>$destination1->id,
            "vote"=>3
        ]);

        Note::create([
            "user_id"=>$utilisateur1->id,
            "destination_id"=>$destination2->id,
            "vote"=>5
        ]);

        $commande1=Commande::create([
            "users_id"=>1,
            "etat"=>"en préparation",

        ]);

        DestinationCommande::create([
            "commande_id"=>$commande1->id,
            "destination_id"=>$destination1->id,
            "nbPlaces"=>3
        ]);

        DestinationCommande::create([
            "commande_id"=>$commande1->id,
            "destination_id"=>$destination2->id,
            "nbPlaces"=>5
        ]);

        Commentaire::create([
            "destination_id"=>1,
            "user_id"=>1,
            "texte"=>"Super destination !",
        ]);

        Commentaire::create([
            "destination_id"=>1,
            "user_id"=>1,
            "texte"=>"Super voyages ! Des images inoubliables",
        ]);

        // \App\Models\User::factory(10)->create();
    }
}
