<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class DestinationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [

                "nom"=>$this->faker->word()." ".$this->faker->unique()->country(),
                "prix"=>$this->faker->randomFloat(2,0,999),
                "estDisponible"=>true,
                "pays_id"=>1

        ];
    }
}
