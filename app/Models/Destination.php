<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    use HasFactory;

    protected $guarded = ["id"];

    public function calcNote()
    {
        $moyenne = 0;
        if (count($this->notes) != 0) {
            foreach ($this->notes as $uneNote) {
                $moyenne += $uneNote->pivot->vote;
            }
            $moyenne = $moyenne / count($this->notes);
        }
        else{
            $moyenne=2.5;
        }
        return $moyenne;
    }

    public function pays()
    {
        return $this->belongsTo(Pays::class);
    }

    public function notes()
    {
        return $this->belongsToMany(User::class, "note")->using(Note::class)->withPivot("vote")->withTimestamps();
    }

    public function commentaires()
    {
        return $this->hasMany(Commentaire::class);
    }

    public function lignes()
    {
        return $this::belongsToMany(Commande::class, "destination_commande")->using(DestinationCommande::class)->withPivot("nbPlaces")->withTimestamps();
    }
}
