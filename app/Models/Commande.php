<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;
    protected $guarded=["id"];

    public function utilisateur(){
        return $this::belongTo(User::class,"users_id");
    }

    public function lignes(){
        return $this::belongsToMany(Destination::class,"destination_commande")->using(DestinationCommande::class)->withPivot("nbPlaces")->withTimestamps();
    }

    public function calcTotal(){
        $resultat=0;
        foreach($this->lignes as $uneLigne){
            $resultat=$uneLigne->prix*$uneLigne->pivot->nbPlaces;
        }
        return $resultat;
    }

}
