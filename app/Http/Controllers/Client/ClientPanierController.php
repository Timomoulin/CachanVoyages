<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Destination;
use Darryldecode\Cart\Facades\CartFacade;
use Illuminate\Http\Request;

class ClientPanierController extends Controller
{

    public function index()
    {
        $panier = CartFacade::getContent();
        return view("client.panier.index", ["panier" => $panier]);
    }
    /**
     * Ajouter une ligne au panier
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {

        $attributs = $request->validate([
            "idDestination" => "required|exists:destinations,id",
            "places" => "required|min:1|max:20"
        ]);

        $idUser = auth()->user()->id;

        $destination = Destination::findOrFail($request->idDestination);
        //Si la destination est deja dans le panier
        if (CartFacade::get($destination->id) !== null) {
            //On ajuster la quantite
            CartFacade::update($destination->id, [
                'quantity' => $request->places,
            ]);
        } else {
            //Sinon on ajoute la destination au panier
            CartFacade::add(array(
                'id' => $destination->id,
                'name' => $destination->nom,
                'price' => $destination->prix,
                'quantity' => $request->places,
                'attributes' => array(),
                'associatedModel' => $destination
            ));
        }
        session()->flash("success","Ajout de ".$destination->nom." dans votre panier");
        return redirect("client/panier");
    }


    public function update(Request $request, Destination $panier)
    {

        $request->validate([
            "places" => "required|numeric|min:0|max:20"
        ]);
        if ($request->places <= 0) {
            CartFacade::remove($panier->id);
        } else {
            CartFacade::update($panier->id, ["quantity" => [
                "value" => $request->places,
                "relative" => false
            ]]);

        }
        session()->flash("success","Modification de votre panier");
        return redirect("/client/panier");
    }

    public function destroy(Destination $panier){
        CartFacade::remove($panier->id);
        session()->flash("success","Une destination supprimer");
        return redirect("/client/panier");
    }

    public function vider(){
        CartFacade::clear();
        session()->flash("success","Votre panier est vider");
        return redirect("/client/panier");
    }
}
