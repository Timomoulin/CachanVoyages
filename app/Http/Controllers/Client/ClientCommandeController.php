<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Commande;
use App\Models\DestinationCommande;
use Darryldecode\Cart\Facades\CartFacade;
use Illuminate\Http\Request;

class ClientCommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utilisateur=auth()->user();
        //Recupere les commandes d'un utilisateur
        $commandes=Commande::select("*")->where("users_id","=",$utilisateur->id)->get();
        return view("client.commandes.index",["commandes"=>$commandes]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO Validation
        $panier = CartFacade::getContent();
        $commande=Commande::create([
            "users_id"=>auth()->user()->id,
            "etat"=>"en préparation",
        ]);
        foreach($panier as $uneLigne){
            DestinationCommande::create([
                "commande_id"=>$commande->id,
                "destination_id"=>$uneLigne->id,
                "nbPlaces"=>$uneLigne->quantity
            ]);
        }
        CartFacade::clear();
        session()->flash("success","La commande est enregistrer");
        return redirect("/client/commandes");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function show(Commande $commande)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function edit(Commande $commande)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commande $commande)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commande $commande)
    {
        //
    }
}
