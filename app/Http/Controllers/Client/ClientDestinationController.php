<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\Destination;
use App\Models\Note;
use App\Models\Pays;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Stringable;

class ClientDestinationController extends Controller
{

public function vote(Destination $destination,Request $request){

    $attributs=$request->validate([
        "vote"=>"required|numeric|min:0|max:5"
    ]);
    $utilisateur=auth()->user();
    $attributs["destination_id"]=$destination->id;
    $attributs["user_id"]=$utilisateur->id;

    Note::create($attributs);
    return redirect()->back();
}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
 //Permet Eager Loading permet d'optimiser le chargement des associations
// L'oposer est Lazy Loading qui se qu'il se passe par défaut ( tres bien) si il n'y pas besoin d'utiliser les autres associations
        $querry=Destination::with(["pays","notes"]);
        if($request->input("pays")){
            $querry=$querry->where("pays_id","=",$request->input("pays"));
        }
        if($request->input("nom")){
            $querry=$querry->where("nom","like","%".$request->input("nom")."%");
        }
        $destinations=$querry->paginate(12);
        $pays=Pays::all();
        return view("client.destinations.index",["lesDestinations"=>$destinations,"lesPays"=>$pays]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function show(Destination $destination)
    {
        // $destination->with(["commentaires","notes","pays"]);
        return view("client.destinations.show",["laDestination"=>$destination]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function edit(Destination $destination)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Destination $destination)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Destination  $destination
     * @return \Illuminate\Http\Response
     */
    public function destroy(Destination $destination)
    {
        //
    }
}
