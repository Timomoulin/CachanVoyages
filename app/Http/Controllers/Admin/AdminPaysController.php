<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Pays;
use Illuminate\Http\Request;

class AdminPaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //Dans la variable lesPays on recupere tout les pays de la bdd
    $lesPays=Pays::with("destinations")->get();
    // $lesPays=Pays::all();
    //Je retourne la vue pays.blade.php en transmettant $lesPays associer a la chaine dataPays
    return view("admin.pays.index",["dataPays"=>$lesPays]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.pays.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes=$request->validate([
            "nom"=>"required|min:2|max:150|unique:pays,nom",
            "region"=>"required",
            "capital"=>"required|min:3",
            "drapeau"=>"image"
        ]);


        $attributes["drapeau"]=$request->file("drapeau")->store("pays");

        Pays::create($attributes);
        session()->flash("success","Le pays a bien était ajouter");
        return redirect("/admin/pays");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function show(Pays $pays)
    {
        //$pays coresond au pays qui a pour id l'id indiquer dans l'url
        //si l'id indiquer ne correspond pas a un pays on a une page 404
        return view("admin.pays.show",["pays"=>$pays]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function edit(Pays $pays)
    {
        return view("admin.pays.edit",["pays"=>$pays]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pays $pays)
    {
        $attributes=$request->validate([
            "nom"=>"required|min:2|max:150|unique:pays,nom,".$pays->id.",id",
            "region"=>"required",
            "capital"=>"required|min:3",
            "drapeau"=>"image"
        ]);

        if(isset($attributes["drapeau"])){
            $attributes["drapeau"]=$request->file("drapeau")->store("pays");
        }
        //Mettre a jour dans la bdd le pays avec les nouvelles de ses attributs.
        $pays->update($attributes);
        session()->flash("success","Le pays a bien était modifier");
        return redirect("/admin/pays");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pays $pays)
    {
        $pays->delete();
        session()->flash("success","Le pays a bien était supprimer");
        return redirect("/admin/pays");
    }
}
