<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Affiche le formulaire de contacte
     *
     * @param Request $request
     * @return void
     */
    public function formulaireContact(Request $request){
        return view("client.contact");
    }

    /**
     * Traitement du formulaire de contacte
     *
     * @param Request $request
     * @return void
     */
    public function traitementContact(Request $request){
        $data=$request->validate([
            "email"=>"required|email",
            "sujet"=>"required|min:2|max:255",
            "texte"=>"required|max:255",
            'captcha' => 'required|captcha'
        ]);
        Mail::to('timotheemoulin01@gmail.com')->send(new Contact($data["email"],$data["sujet"],$data["texte"]));
        // $envoi=new Contact($attributs["email"],$attributs["sujet"],$attributs["texte"]);
        // Mail::send($envoi);


    }

    /**
     * Recharge un nouveau captcha
     *
     * @return void
     */
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

}
