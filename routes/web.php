<?php

use App\Http\Controllers\Admin\AdminDestinationController;
use App\Http\Controllers\Admin\AdminPaysController;
use App\Http\Controllers\Client\ClientCommandeController;
use App\Http\Controllers\Client\ClientCommentaireController;
use App\Http\Controllers\Client\ClientDestinationController;
use App\Http\Controllers\Client\ClientPanierController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PaysController;
use App\Models\Pays;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Quand on contacte /pays on declenche la méthode index de PaysController
// Route::get('/pays',[PaysController::class,"index"]);

// Route::get('/pays/create',[PaysController::class,"create"]);

// Route::post('/pays',[PaysController::class,"store"]);
// //{pays} corespond a l'id d'un pays
// Route::get('/pays/{pays}',[PaysController::class,"show"]);

// Route::get('/pays/{pays}/edit',[PaysController::class,"edit"]);

// Route::put('/pays/{pays}',[PaysController::class,"update"]);

// Route::delete('/pays/{pays}',[PaysController::class,"destroy"]);

//Tout les mappings en seul par cela suis la même syntaxe pour les URL
//Route::resource("/pays",AdminPaysController::class)->parameters(["pays"=>"pays"])->middleware("estAdmin");
//Route::resource("/destinations",AdminDestinationController::class)->parameters(["destination"=>"destination"])->middleware("estAdmin");

//Alternative pour regrouper les routes par middleware sinon voir au dessus
Route::middleware("estAdmin")->group(
    function(){
    Route::resource("/admin/pays",AdminPaysController::class)->parameters(["pays"=>"pays"]);
    Route::resource("/admin/destinations",AdminDestinationController::class)->parameters(["destination"=>"destination"]);

    }
);

Route::middleware("auth")->group(
    function(){

//Les routes du panier
Route::resource("client/panier",ClientPanierController::class)->parameters(["panier"=>"panier"]);
Route::delete("client/panier",[ClientPanierController::class,"vider"]);

//Les routes des commandes (pour les clients)
Route::resource("client/commandes",ClientCommandeController::class)->parameters(["commande"=>"commande"])->only(["index","show","store"]);

//Les routes des commentaires
Route::resource("client/commentaires",ClientCommentaireController::class)->parameters(["commentaire"=>"commentaire"])->except(["show"]);

//Route pour voter pour une destination
Route::post("client/destinations/{destination}/vote",[ClientDestinationController::class,"vote"]);

}


);
Route::resource("pays",PaysController::class)->only(["index","show"]);
Route::resource("destinations",ClientDestinationController::class)->parameters(["destination"=>"destination"]);



Route::get("/test/dashboard",function(){return view("admin.index");});
Route::get("/contacte",[HomeController::class,"formulaireContact"]);
Route::post("/contacte",[HomeController::class,"traitementContact"]);
Route::get('refreshcaptcha',[HomeController::class, 'refreshCaptcha'])->name('refreshcaptcha');

//le middleware auth verifie que l'utilsateur est authentifié.
Route::get("/dashboard",function(){
    return view("client.dashboard");
})->middleware("auth");

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

//Langues
Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'App\Http\Controllers\LanguageController@switchLang']);
